<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $curl = curl_init();

        if (!$curl) {
            die("Couldn't initialize a cURL handle");
        }

        // Set the file URL to fetch through cURL
        curl_setopt($curl, CURLOPT_URL, "https://api.themoviedb.org/3/movie/popular?api_key=531eaffcac14a8c431f91d7a77a345e8");

        // Set a different user agent string (Googlebot)
        curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)');

        // Follow redirects, if any
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        // Fail the cURL request if response code = 400 (like 404 errors)
        curl_setopt($curl, CURLOPT_FAILONERROR, true);

        // Return the actual result of the curl result instead of success code
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // Wait for 30 seconds to connect, set 0 to wait indefinitely
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);

        // Execute the cURL request for a maximum of 50 seconds
        curl_setopt($curl, CURLOPT_TIMEOUT, 50);

        // Do not check the SSL certificates
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // Fetch the URL and save the content in $html variable
        $data = curl_exec($curl);
        $movies = [];

        // Check if any error has occurred
        if (curl_errno($curl))
        {
            echo 'cURL error: ' . curl_error($curl);
        }
        else
        {
            // cURL executed successfully
            $result = json_decode($data);
            //dd($result->results);
            $movies = $result->results;
        }

        // close cURL resource to free up system resources
        curl_close($curl);
        //dd($movies);
        return view('home', compact('movies'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/details/{id}', 'HomeController@details')->name('details');
Route::post('/search', 'HomeController@search');
Route::post('/list', 'WatchLaterController@add');
Route::get('/list', 'WatchLaterController@getList');
Route::post('/list/{id}', 'WatchLaterController@delete');
