@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="/search" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" placeholder="Search movie by title">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <h3>Search Results</h3>
    <div class="row mt-3">
        @foreach($movies as $movie)
        <div class="col-4">
            <div class="card"> 
               <div class="card-header">
                  <img class="card-img" src="https://image.tmdb.org/t/p/w500/{{ $movie->poster_path }}" alt="Card image">
               </div>  
               <div class="card-body">
                  <h3 class="card-title">{{ $movie->title }}</h3>
                  <div class="container">
                     <div class="row">
                        <div class="col-4 metadata">
                           <i class="fa fa-star" aria-hidden="true"></i> 
                           <p>Rating: {{ $movie->vote_average }}</p>
                        </div>
                        <div class="col-8 metadata"><a class="btn btn-default" href="details/{{ $movie->id }}">More Info</a></div>
                     </div>
                  </div>      
                  <p class="card-text" id="overview" style="overflow: hidden;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">{{ $movie->overview }}</p>
                  <a class="trailer-preview" href="https://youtu.be/ePbKGoIGAXY" target="new">
                     <i class="fa fa-play" aria-hidden="true"></i>
                     </a>
               </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
