@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
            <div class="row">
            <div class="col-md-4">
              <img class="card-img" id="poster_path" src="https://image.tmdb.org/t/p/w500/{{ $movie->poster_path }}" alt="Poster">
            </div>
            <div class="col-md-8">
              <p id="id" hidden>{{ $movie->id }}</p>
              <h2 id="title">{{ $movie->title }} ({{ date('Y', strtotime($movie->release_date)) }})</h2>
              @foreach($movie->genres as $genre)
                <span class="badge badge-secondary">{{$genre->name}} </span>
              @endforeach
              <button type="button" class="btn btn-default" onclick="addToList()">Watch Later</button>
             <h3>Overview</h3>
             <p id="overview">{{ $movie->overview }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
