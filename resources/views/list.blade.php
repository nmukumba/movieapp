@extends('layouts.app')

@section('content')
<div class="container">
    <h3>My List</h3>
    <div class="row justify-content-center mt-3">
        @foreach($movies as $movie)
        <div class="col-4">
            <div class="card"> 
               <div class="card-header">
                  <img class="card-img" src="https://image.tmdb.org/t/p/w500/{{ $movie->poster_path }}" alt="Card image">
               </div>  
               <div class="card-body">
                  <h3 class="card-title">{{ $movie->title }}</h3>
                  <div class="container">
                     <div class="row">
                        <div class="col-4 metadata">
                          <form action="/list/{{ $movie->id }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button>
                          </form> 
                        </div>
                        <div class="col-8 metadata">
                          <a class="btn btn-default pull-right" href="/details/{{ $movie->movie_id }}">More Info</a>
                        </div>
                     </div>
                  </div>      
                  <p class="card-text mt-2" id="overview" style="overflow: hidden;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">{{ $movie->overview }}</p>
               </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
