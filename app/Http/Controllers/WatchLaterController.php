<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class WatchLaterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Request $request) {
    	$data = request()->validate([
            'movie_id' => 'required',
            'title' => 'required',
            'overview' => 'required',
            'poster_path' => 'required',
        ]);

        if (Auth::check()) {
        	$user_id = Auth::id();
        	$movie = DB::table('watch_later_list')
        			->select('watch_later_list.*')
                    ->where([['movie_id', '=', $data['movie_id']], ['user_id', '=', $user_id]])
                    ->get();

             if ($movie->count() > 0) {
             	 return json_encode(['status'=>false, 'message'=> 'Movie already on the list.']);
             } else {
             	DB::table('watch_later_list')->insert(array_merge($data, ["user_id" => $user_id]));
             	return json_encode(['status'=>true, 'message'=> 'Movie added to list.']);
             }
       
        } else {
        	return json_encode(['status'=>false, 'message'=> 'You must be logged in']);
        }
       
    }

    public function getList(){
        $user_id = Auth::id();
        $movies = DB::table('watch_later_list')
                    ->select('watch_later_list.*')
                    ->where('user_id', '=', $user_id)
                    ->get();

        return view('list', compact('movies'));
    }

    public function delete($id){
        DB::table('watch_later_list')->where('id', '=', $id)->delete();
        return redirect('/list');
    }
}
