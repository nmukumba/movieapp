
function addToList() {
  var id = $('#id').text();
  var title = $('#title').text();
  var overview = $('#overview').text();
  var poster_path = $('#poster_path').attr('src');

  $.ajax({
    type: "POST",
    url: "/list",
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    dataType: "json",
    data: { movie_id: id, title: title, overview: overview, poster_path: poster_path },
    success: function(data){
      if (data.status === true) {
        toastr.success(data.message);
      } else {
        toastr.error(data.message);
      }
      
    },
    error: function(error) {
      console.log(error);
    }
  });
}